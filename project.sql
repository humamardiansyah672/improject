/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : project

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2023-08-07 08:44:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `nik` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `c_aybs` int(7) NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '74ee55083a714aa3791f8d594fea00c9',
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `superuser` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `ID_GROUP` int(11) NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `atasan` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `exprd` date NOT NULL,
  `salah` int(2) NOT NULL DEFAULT 0,
  `c_areacode` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `dt_area` text COLLATE latin1_general_ci NOT NULL,
  `c_comcode` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `c_kdterima` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `c_areacodepac` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `dt_areapac` text COLLATE latin1_general_ci NOT NULL,
  `c_areacoderibindo` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `dt_arearibindo` text COLLATE latin1_general_ci NOT NULL,
  `c_otp` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `l_verified` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `exp_otp` date NOT NULL,
  `l_otp` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`nik`),
  KEY `nik` (`nik`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('12345', '0', '74ee55083a714aa3791f8d594fea00c9', 'test', 'user', 'N', '0', 'xxx@email.com', 'N', 'N', '2123-08-07', '0', '', '', '', '', '', '', '', '', '', 'N', '0000-00-00', 'N');
