<?php
include "config/koneksi.php";
include "config/fungsi_log.php";
include "config/cekIp.php";

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

function rand_string( $length ) {
   $chars = "0123456789"; 

   $str = '';

   $size = strlen( $chars );
   for( $i = 0; $i < $length; $i++ ) {
     $str .= $chars[ rand( 0, $size - 1 ) ];
   }

   return $str;
}

date_default_timezone_set('Asia/Jakarta');
$username = $_POST['username'];
$pass     = md5($_POST['password']);
$blokir   = 'N';
$otp      = rand_string(5);

$client  = @$_SERVER['HTTP_CLIENT_IP'];
$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
$remote  = $_SERVER['REMOTE_ADDR'];
$browser = $_SERVER['HTTP_USER_AGENT'];

if(filter_var($client, FILTER_VALIDATE_IP)){
    $ip = $client;
}
elseif(filter_var($forward, FILTER_VALIDATE_IP)){
    $ip = $forward;
}else{
    $ip = $remote;
}


$ketemu = $db->prepare("SELECT * FROM admins WHERE nik=:username AND password=:pass AND blokir=:blokir");
$ketemu->bindParam(':username', $username, PDO::PARAM_STR);
$ketemu->bindParam(':pass', $pass, PDO::PARAM_STR);
$ketemu->bindParam(':blokir', $blokir, PDO::PARAM_STR);
$ketemu->execute();
$r = $ketemu->fetch();
$s = $ketemu->rowCount();
$tanggal=date('Y-m-d h:i:sa');
$tanggal2=date('Y-m-d');


// Apabila username dan password ditemukan
if ($s > 0){


       $date1=$r['exprd'];
       $pass=$r['password'];
       $limit=date('Y-m-d', strtotime('-3 days',strtotime($date1))) ;
       
       

       if ($r['exprd'] <= date("Y-m-d")) {
       echo "<script>alert('Password Expired')
              javascript:history.go(-1)</script>";
       }
       elseif ($limit <= date("Y-m-d") )  {
       session_start();

       $_SESSION['nik']          = $r['nik'];
       $_SESSION['c_areacode']   = $r['c_areacode'];
       $_SESSION['namalengkap']  = $r['nama_lengkap'];
       $_SESSION['passuser']     = $r['password'];
       $_SESSION['leveluser']    = $r['level'];
       $_SESSION['atasan']       = $r['atasan'];
       $_SESSION['idgroup']    = $r['ID_GROUP'];
       $_SESSION['superuser']    = $r['superuser'];
       $_SESSION['c_kdterima']   = $r['c_kdterima'];
       $_SESSION['dt_area']      = $r['dt_area'];
       setcookie('nik', $r['nik']);
       $_SESSION['l_verified']      = 'Y';

       echo " <script>alert('Silahkan Rubah Password !');
              window.location.href='../project/media.php?module=home'</script>";
              //header('location:media.php?module=password');
       }
       elseif ($_GET['url']!=''){
       session_start();

       $_SESSION['nik']          = $r['nik'];
       $_SESSION['c_areacode']   = $r['c_areacode'];
       $_SESSION['namalengkap']  = $r['nama_lengkap'];
       $_SESSION['passuser']     = $r['password'];
       $_SESSION['leveluser']    = $r['level'];
       $_SESSION['atasan']       = $r['atasan'];
       $_SESSION['idgroup']    = $r['ID_GROUP'];
       $_SESSION['superuser']    = $r['superuser'];
       $_SESSION['c_kdterima']   = $r['c_kdterima'];
       $_SESSION['dt_area']      = $r['dt_area'];
       setcookie('nik', $r['nik']);
       $_SESSION['l_verified']      = 'Y';

       echo " <script>
              window.location.href='$_GET[url]'</script>";
       
       }else{
       session_start();

       $_SESSION['nik']          = $r['nik'];
       $_SESSION['c_areacode']   = $r['c_areacode'];
       $_SESSION['namalengkap']  = $r['nama_lengkap'];
       $_SESSION['passuser']     = $r['password'];
       $_SESSION['leveluser']    = $r['level'];
       $_SESSION['atasan']       = $r['atasan'];
       $_SESSION['idgroup']    = $r['ID_GROUP'];
       $_SESSION['superuser']    = $r['superuser'];
       $_SESSION['c_kdterima']   = $r['c_kdterima'];
       $_SESSION['dt_area']      = $r['dt_area'];
       setcookie('nik', $r['nik']);
       $_SESSION['l_verified']      = 'Y';
       
       header('location:media.php?module=home');
       
       }


}
else{
  tambahlog($username,'CEKLOGIN','LOGIN','GAGAL');

  echo "<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.5 -->
  <link rel='stylesheet' href='bootstrap/css/bootstrap.css'>
  <!-- Font Awesome -->
    <!-- Theme style -->
  <link rel='stylesheet' href='dist/css/AdminLTE.css'>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
 <link rel='stylesheet' href='dist/css/skins/_all-skins.min.css'>
 <center>LOGIN GAGAL! <br> 
        Username atau Password Anda tidak benar.<br>
        Atau account Anda sedang diblokir.<br>";
  echo "<a href=index.php><b>ULANGI LAGI</b></a></center>  ";
}
?>
