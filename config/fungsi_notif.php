<?php

function smsnotifsp($nomer,$c_spno) {

    include "config/fungsi_encryptdecrypt.php";

    $dbhost = "10.11.12.17";
    $dbuser = "test";
    $dbpass = "alhidayah";
    $dbname = "sms";
    
    $db2 = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
    $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $alamat = 'http://sit-ig.net/plant/tracking.php?id=';
    $id     = encrypt_decrypt('encrypt',$c_spno);
    $message  = 'Customer Yth, Pesanan anda sudah kami terima, gunakan link berikut untuk melihat status pesanan : '.$alamat.$id.' Terima Kasih PT. Immortal Cosmedika Indonesia';
    // menghitung jumlah pecahan
    $jmlSMS = ceil(strlen($message)/153);
    // memecah pesan asli
    $pecah  = str_split($message, 153);
    $tampil = $db2->prepare("SHOW TABLE STATUS LIKE 'outbox'");
    $tampil->execute();
    $data = $tampil->fetch();
    $newID = $data['Auto_increment'];

    if($jmlSMS==1){

        $simpan = $db2->prepare("INSERT INTO outbox(DestinationNumber, 
                                                    TextDecoded, 
                                                    CreatorID) 
                                            VALUES ('$nomer', 
                                                    '$message', 
                                                    'Gammu')");
        $simpan->execute();

    }else{

        // proses penyimpanan ke tabel mysql untuk setiap pecahan        
        for ($i=1; $i<=$jmlSMS; $i++)
        {
            // membuat UDH untuk setiap pecahan, sesuai urutannya
            $udh = "050003A7".sprintf("%02s", $jmlSMS).sprintf("%02s", $i);
            // membaca text setiap pecahan
            $msg = $pecah[$i-1];
            
            if ($i == 1) {
                // jika merupakan pecahan pertama, maka masukkan ke tabel OUTBOX
                $query = "INSERT INTO outbox (DestinationNumber, UDH, TextDecoded, ID, MultiPart, CreatorID)
                            VALUES ('$nomer', '$udh', '$msg', '$newID', 'true', 'Gammu')";
            }
            else{
                // jika bukan merupakan pecahan pertama, simpan ke tabel OUTBOX_MULTIPART
                $query = "INSERT INTO outbox_multipart(UDH, TextDecoded, ID, SequencePosition)
                            VALUES ('$udh', '$msg', '$newID', '$i')";           
            }
            
            // jalankan query                              
            $simpan = $db2->prepare($query);
            $simpan->execute(); 
        }

    }

    $db2 = null;

}

function smsnotiffaktur($nomer,$c_invno){

    include "config/fungsi_encryptdecrypt.php";
    
    $dbhost = "10.11.12.17";
    $dbuser = "test";
    $dbpass = "alhidayah";
    $dbname = "sms";
    
    $db2 = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
    $db2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $alamat = 'http://sit-ig.net/plant/trackingfaktur.php?id=';
    $id     = encrypt_decrypt('encrypt',$c_invno);
    $message  = 'Customer Yth, Pesanan anda akan segera kami kirimkan, gunakan link berikut untuk melihat detail faktur : '.$alamat.$id.' Terima Kasih PT. Immortal Cosmedika Indonesia';
    // menghitung jumlah pecahan
    $jmlSMS = ceil(strlen($message)/153);
    // memecah pesan asli
    $pecah  = str_split($message, 153);
    $tampil = $db2->prepare("SHOW TABLE STATUS LIKE 'outbox'");
    $tampil->execute();
    $data = $tampil->fetch();
    $newID = $data['Auto_increment'];

    if($jmlSMS==1){

        $simpan = $db2->prepare("INSERT INTO outbox(DestinationNumber, 
                                                    TextDecoded, 
                                                    CreatorID) 
                                            VALUES ('$nomer', 
                                                    '$message', 
                                                    'Gammu')");
        $simpan->execute();

    }else{

        // proses penyimpanan ke tabel mysql untuk setiap pecahan        
        for ($i=1; $i<=$jmlSMS; $i++)
        {
            // membuat UDH untuk setiap pecahan, sesuai urutannya
            $udh = "050003A7".sprintf("%02s", $jmlSMS).sprintf("%02s", $i);
            // membaca text setiap pecahan
            $msg = $pecah[$i-1];
            
            if ($i == 1) {
                // jika merupakan pecahan pertama, maka masukkan ke tabel OUTBOX
                $query = "INSERT INTO outbox (DestinationNumber, UDH, TextDecoded, ID, MultiPart, CreatorID)
                            VALUES ('$nomer', '$udh', '$msg', '$newID', 'true', 'Gammu')";
            }
            else{
                // jika bukan merupakan pecahan pertama, simpan ke tabel OUTBOX_MULTIPART
                $query = "INSERT INTO outbox_multipart(UDH, TextDecoded, ID, SequencePosition)
                            VALUES ('$udh', '$msg', '$newID', '$i')";           
            }
            
            // jalankan query                              
            $simpan = $db2->prepare($query);
            $simpan->execute(); 
        }

    }

    $db2 = null;

   
}

?>